-- Check access token 
-- Written by Kevin.XU
-- 2016/7/25

--[[
Request body:::
{
    "service_type":"SHOPPING",
    "token":"eyJleHBpcmVfdGltZSI6MTQ2OTE4OTMxMSwibWVtYmVyX2xldmVsIjoiMSIsImxvZ2luX2F1dGgiOiJqYWNrIiwidmFsaWRpdHkiOjMwLCJjbGllbnRfaXAiOiIxMTIuMTEuMS4xMCIsImNsaWVudF90eXBlIjoiaXBob25lIn0="
}
Response body:::
{
    "status":0,
    "status_desc":""
}
]]

ngx.req.read_body()

local resty_redis = require "resty.redis"
local resty_cjson = require "cjson"
local resty_md5 = require "resty.md5"
local resty_string = require "resty.string"
local limit_toolkit = require "ddtk.limit_tk"
local redis_config = require "ddtk.redis_config"

local max_idle_timeout = 300*1000
local pool_size = 20
local timeout = 1000

--make response
local resp={}

--read request body and make token value
local now=os.time()
local json_request_body_data = ngx.req.get_body_data()
local request_body = resty_cjson.decode(json_request_body_data) 
local service_type = request_body.service_type
local token_value = request_body.token


local tkey_hex = limit_toolkit.md5(token_value)
if not tkey_hex then
    ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
    return
end

--get redis address
local redis_ip,redis_port = redis_config.select_slave_redis_node(service_type)
if redis_ip == nil then
    ngx.log(ngx.ERR, "not found redis")
    ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
    return
end
local red = resty_redis:new()
red:set_timeout(timeout)
local ok, err = red:connect(redis_ip, redis_port)
if not ok then
    ngx.log(ngx.ERR, "connect failed")
    ngx.exit(ngx.HTTP_INTERNAL_SERVER_ERROR)
    return
end
--try to find token value from redis
local res, err2 = red:get(tkey_hex)
if res == ngx.null then
    resp['status']=1
    resp['status_desc']='Invalid token'
else
    resp['status']=0
    resp['status_desc']='Valid token'
end

--keep connection alive
red:set_keepalive(max_idle_timeout, pool_size)


local resp_json = resty_cjson.encode(resp) 


ngx.say(resp_json)

