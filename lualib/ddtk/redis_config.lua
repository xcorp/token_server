-- Config of redis nodes 
-- Written by Kevin.XU
-- 2016/7/25


local _M = {
    _VERSION = '0.1'
}

-- config master redis node at local DC
local data = {
    ["SHOPPING"] = {
        ["master_redis_node"] = {
            '127.0.0.1',6379
        },
        ["slave_redis_nodes"] = {
            {'127.0.0.1',6379}
        }
    },
    ["DEAL"] = {
        ["master_redis_node"] = {
            '127.0.0.1',6380
        },
        ["slave_redis_nodes"] = {
            {'127.0.0.1',6380}
        }
    }
}


-- select master redis node
function _M.select_master_redis_node(service_type)
    local master = data[service_type].master_redis_node
    return master[1], master[2]
end


-- select one slave redis node
function _M.select_slave_redis_node(service_type)
    local slaves = data[service_type].slave_redis_nodes
    local size = table.getn(slaves)
    if size == 0 then
        return nil
    end
    local t = os.time()
    local idx = (t % size) + 1
    return slaves[idx][1],slaves[idx][2]
end


return _M